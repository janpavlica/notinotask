# NotinoTask

Native iOS application for displaying a couple of Notino products.

## Features

- Clean architecture
- Unit and UI tests
- No 3rd party libraries

## Requirements

- iOS 15.0+
- Xcode 11.0

## Author

**Jan Pavlica**\
palivko@gmail.com / [LinkedIn](https://www.linkedin.com/in/jan-pavlica/)

Distributed under the MIT license. See ``LICENSE`` for more information.