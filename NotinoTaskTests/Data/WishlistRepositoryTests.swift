//
// Created by Jan Pavlica on 13.12.2022.
//

import XCTest
@testable import NotinoTask

final class WishlistRepositoryTests: XCTestCase {

    private let userDefaults = UserDefaults(suiteName: "WishlistRepositoryTests")!
    private var wishlistStorage: WishlistStorage!
    private var wishlistRepository: WishlistRepository!
    private var product: Product!

    override func setUp() {
        super.setUp()
        wishlistStorage = DefaultWishlistStorage(userDefaults: userDefaults)
        wishlistRepository = DefaultWishlistRepository(storage: wishlistStorage)

        wishlistStorage.removeAll()
    }

    func testProductsInWishlist() {
        let product = MockUtils.getProductDTO().toDomain()
        wishlistRepository.addToWishlist(product: product)
        let count = wishlistRepository.getWishlist().count
        XCTAssertEqual(count, 1)
    }

    func testAddProductToWishlist() {
        let product = MockUtils.getProductDTO().toDomain()
        wishlistRepository.addToWishlist(product: product)
        let productInWishlist = wishlistRepository.getWishlist().first!
        XCTAssertEqual(productInWishlist.id, product.id)
    }

    func testAddExistingProductToWishlist() {
        let product = MockUtils.getProductDTO().toDomain()
        wishlistRepository.addToWishlist(product: product)
        wishlistRepository.addToWishlist(product: product)
        let count = wishlistRepository.getWishlist().count
        XCTAssertEqual(count, 1)
    }

    func testRemoveProductFromWishlist() {
        let product = MockUtils.getProductDTO().toDomain()
        wishlistRepository.addToWishlist(product: product)
        wishlistRepository.removeFromWishlist(product: product)
        let count = wishlistRepository.getWishlist().count
        XCTAssertEqual(count, 0)
    }

    func testRemoveNonExistentProductFromWishlist() {
        let products = MockUtils.getProducts()
        wishlistRepository.addToWishlist(product: products.first!)
        wishlistRepository.removeFromWishlist(product: products.last!)
        let count = wishlistRepository.getWishlist().count
        XCTAssertEqual(count, 1)
    }
}
