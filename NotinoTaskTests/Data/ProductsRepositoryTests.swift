//
// Created by Jan Pavlica on 08.12.2022.
//

import XCTest
@testable import NotinoTask

final class ProductsRepositoryTests: XCTestCase {

    private var shopAPI: MockShopAPI!
    private var productsRepository: ProductsRepository!

    override func setUp() {
        super.setUp()
        shopAPI = MockShopAPI()
        productsRepository = DefaultProductsRepository(api: shopAPI, imageServerUrl: "")
    }

    func testProductDomainMapping() async throws {
        let productDTO = MockUtils.getProductDTO()
        let products = try await productsRepository.getProducts()
        let product = products.first(where: { $0.id == productDTO.productId })!
        XCTAssertEqual(product.id, productDTO.productId)
        XCTAssertEqual(product.name, productDTO.name)
        XCTAssertEqual(product.description, productDTO.annotation)
        XCTAssertEqual(product.brand.id, productDTO.brand.id)
        XCTAssertEqual(product.brand.name, productDTO.brand.name)
        XCTAssertEqual(product.price.value, productDTO.price.value)
        XCTAssertEqual(product.price.currency, productDTO.price.currency)
        XCTAssertEqual(product.imageUrl, productDTO.imagePath)
    }

    func testProductImageUrl() async throws {
        let imageServerUrl = "https://example.com/"
        productsRepository = DefaultProductsRepository(api: shopAPI, imageServerUrl: imageServerUrl)
        let productDTO = MockUtils.getProductDTO()
        let products = try await productsRepository.getProducts()
        let product = products.first(where: { $0.id == productDTO.productId })!
        XCTAssertEqual(product.imageUrl, imageServerUrl + productDTO.imagePath)
    }

    func testProductsCount() async throws {
        let count = try await shopAPI.getProducts().vpProductByIds.count
        let products = try await productsRepository.getProducts()
        XCTAssertEqual(products.count, count)
    }

    func testProductsNotSorted() async throws {
        let productDTO = try await shopAPI.getProducts().vpProductByIds.first!
        let product = try await productsRepository.getProducts().first!
        XCTAssertEqual(product.id, productDTO.productId)
    }
}
