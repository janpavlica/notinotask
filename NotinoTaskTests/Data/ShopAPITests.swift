//
// Created by Jan Pavlica on 08.12.2022.
//

import XCTest
@testable import NotinoTask

final class ShopAPITests: XCTestCase {

    private var products: ProductsDTO!
    private var networking: MockNetworking!
    private var shopAPI: ShopAPI!

    override func setUp() {
        super.setUp()
        products = MockUtils.getProductsResponse()
        networking = MockNetworking()
        shopAPI = DefaultShopAPI(networking: networking)
    }

    func testGetProductsSuccess() async throws {
        networking.result = try .success(JSONEncoder().encode(products))
        let result = try await shopAPI.getProducts()
        XCTAssertEqual(products.vpProductByIds.count, result.vpProductByIds.count)
    }

    func testInvalidUrlFail() async throws {
        shopAPI = DefaultShopAPI(networking: networking, endpointUrl: "")
        do {
            let _ = try await shopAPI.getProducts()
        } catch {
            XCTAssertEqual(error as! NetworkError, NetworkError.invalidURL)
        }
    }

    func testNoResponseFail() async throws {
        networking.result = .failure(NetworkError.noResponse)
        do {
            let _ = try await shopAPI.getProducts()
        } catch {
            XCTAssertEqual(error as! NetworkError, NetworkError.noResponse)
        }
    }

    func testDecodeFail() async throws {
        networking.result = try .success(JSONEncoder().encode(String("")))
        do {
            let _ = try await shopAPI.getProducts()
        } catch {
            XCTAssertEqual(error as! NetworkError, NetworkError.decode)
        }
    }

    func testOfflineFail() async {
        networking.result = .failure(NetworkError.offline)
        do {
            let _ = try await shopAPI.getProducts()
        } catch {
            XCTAssertEqual(error as! NetworkError, NetworkError.offline)
        }
    }

    func testUnknownFail() async {
        networking.result = .failure(NetworkError.unknown)
        do {
            let _ = try await shopAPI.getProducts()
        } catch {
            XCTAssertEqual(error as! NetworkError, NetworkError.unknown)
        }
    }
}
