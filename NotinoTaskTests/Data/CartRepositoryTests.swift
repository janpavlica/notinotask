//
// Created by Jan Pavlica on 13.12.2022.
//

import XCTest
@testable import NotinoTask

final class CartRepositoryTests: XCTestCase {

    private let userDefaults = UserDefaults(suiteName: "CartRepositoryTests")!
    private var cartStorage: CartStorage!
    private var cartRepository: CartRepository!
    private var product: Product!

    override func setUp() {
        super.setUp()
        cartStorage = DefaultCartStorage(userDefaults: userDefaults)
        cartRepository = DefaultCartRepository(storage: cartStorage)

        cartStorage.removeAll()
    }

    func testProductsInCart() {
        let product = MockUtils.getProductDTO().toDomain()
        cartRepository.addToCart(product: product)
        let count = cartRepository.getCart().count
        XCTAssertEqual(count, 1)
    }

    func testAddProductToCart() {
        let product = MockUtils.getProductDTO().toDomain()
        cartRepository.addToCart(product: product)
        let productInCart = cartRepository.getCart().first!
        XCTAssertEqual(productInCart.id, product.id)
    }

    func testAddExistingProductToCart() {
        let product = MockUtils.getProductDTO().toDomain()
        cartRepository.addToCart(product: product)
        cartRepository.addToCart(product: product)
        let count = cartRepository.getCart().count
        XCTAssertEqual(count, 1)
    }

    func testRemoveProductFromCart() {
        let product = MockUtils.getProductDTO().toDomain()
        cartRepository.addToCart(product: product)
        cartRepository.removeFromCart(product: product)
        let count = cartRepository.getCart().count
        XCTAssertEqual(count, 0)
    }

    func testRemoveNonExistentProductFromCart() {
        let products = MockUtils.getProducts()
        cartRepository.addToCart(product: products.first!)
        cartRepository.removeFromCart(product: products.last!)
        let count = cartRepository.getCart().count
        XCTAssertEqual(count, 1)
    }
}
