//
// Created by Jan Pavlica on 14.12.2022.
//

import XCTest
@testable import NotinoTask

final class ProductsUseCaseTests: XCTestCase {

    private let userDefaults = UserDefaults(suiteName: "ProductsUseCaseTests")!
    private var wishlistStorage: WishlistStorage!
    private var cartStorage: CartStorage!
    private var productsRepository: ProductsRepository!
    private var wishlistRepository: WishlistRepository!
    private var cartRepository: CartRepository!
    private var productsUseCase: ProductsUseCase!

    override func setUp() {
        super.setUp()
        wishlistStorage = DefaultWishlistStorage(userDefaults: userDefaults)
        cartStorage = DefaultCartStorage(userDefaults: userDefaults)
        productsRepository = MockProductsRepository()
        wishlistRepository = DefaultWishlistRepository(storage: wishlistStorage)
        cartRepository = DefaultCartRepository(storage: cartStorage)
        productsUseCase = ProductsUseCase(productsRepository: productsRepository,
                wishlistRepository: wishlistRepository, cartRepository: cartRepository)

        wishlistStorage.removeAll()
        cartStorage.removeAll()
    }

    func testProductsCount() async throws {
        let count = MockUtils.getProducts().count
        let productsCount = try await productsUseCase.getProducts().count
        XCTAssertEqual(productsCount, count)
    }

    func testProductWishlistAndCartNotNil() async throws {
        let products = try await productsUseCase.getProducts()
        let product = products.first!
        XCTAssertNotNil(product.inWishlist)
        XCTAssertNotNil(product.inCart)
    }

    func testProductsSortByName() async throws {
        let products = try await productsUseCase.getProducts()
        if products.count >= 3 {
            XCTAssertEqual(products[0].name, "Example product")
            XCTAssertEqual(products[1].name, "Product name")
            XCTAssertEqual(products[2].name, "Some product")
        } else {
            XCTFail("Few products")
        }
    }

    func testProductToggleWishlist() async throws {
        let product = MockUtils.getProductDTO().toDomain()
        productsUseCase.toggleWishlist(product: product)
        var count = wishlistRepository.getWishlist().count
        XCTAssertEqual(count, 1)
        productsUseCase.toggleWishlist(product: product)
        count = wishlistRepository.getWishlist().count
        XCTAssertEqual(count, 0)
    }

    func testProductToggleCart() async throws {
        let product = MockUtils.getProductDTO().toDomain()
        productsUseCase.toggleCart(product: product)
        var count = cartRepository.getCart().count
        XCTAssertEqual(count, 1)
        productsUseCase.toggleCart(product: product)
        count = cartRepository.getCart().count
        XCTAssertEqual(count, 0)
    }
}
