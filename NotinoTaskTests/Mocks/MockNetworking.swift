//
// Created by Jan Pavlica on 08.12.2022.
//

import Foundation
@testable import NotinoTask

class MockNetworking: Networking {
    var result = Result<Data, Error>.success(Data())

    func data(from url: URL, delegate: URLSessionTaskDelegate?) async throws -> (Data, URLResponse) {
        try (result.get(), HTTPURLResponse())
    }
}
