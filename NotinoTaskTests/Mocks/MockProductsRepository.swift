//
// Created by Jan Pavlica on 15.12.2022.
//

import Foundation
@testable import NotinoTask

struct MockProductsRepository: ProductsRepository {

    func getProducts() async throws -> [Product] {
        return MockUtils.getProducts()
    }
}
