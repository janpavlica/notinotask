//
// Created by Jan Pavlica on 14.12.2022.
//

import XCTest
import Combine
@testable import NotinoTask

final class ProductsViewModelTests: XCTestCase {

    private let userDefaults = UserDefaults(suiteName: "ProductsViewModelTests")!
    private var wishlistStorage: WishlistStorage!
    private var cartStorage: CartStorage!
    private var productsRepository: ProductsRepository!
    private var wishlistRepository: WishlistRepository!
    private var cartRepository: CartRepository!
    private var productsUseCase: ProductsUseCase!
    private var viewModel: ProductsViewModel!

    override func setUp() {
        super.setUp()
        wishlistStorage = DefaultWishlistStorage(userDefaults: userDefaults)
        cartStorage = DefaultCartStorage(userDefaults: userDefaults)
        productsRepository = MockProductsRepository()
        wishlistRepository = DefaultWishlistRepository(storage: wishlistStorage)
        cartRepository = DefaultCartRepository(storage: cartStorage)
        productsUseCase = ProductsUseCase(productsRepository: productsRepository,
                wishlistRepository: wishlistRepository, cartRepository: cartRepository)
        viewModel = ProductsViewModel(useCase: productsUseCase)

        wishlistStorage.removeAll()
        cartStorage.removeAll()
    }

    func testInit() {
        XCTAssertEqual(viewModel.products.count, 0)
        XCTAssertEqual(viewModel.state, ProductsViewModelState.loading)
    }

    func testLoadProducts() async throws {

    }
}
