//
// Created by Jan Pavlica on 15.12.2022.
//

import XCTest
@testable import NotinoTask

final class ProductCellViewModelTests: XCTestCase {

    private let formatter = FormatterUtils.currencyFormatter
    private var product: Product!

    override func setUp() {
        super.setUp()
        product = MockUtils.getProduct()
    }

    func testInit() {
        let viewModel = ProductCellViewModel(product: product, currencyFormatter: formatter)
        XCTAssertEqual(viewModel.inWishlist, product.inWishlist ?? false)
        XCTAssertEqual(viewModel.imageUrl, product.imageUrl)
        XCTAssertEqual(viewModel.name, product.name)
        XCTAssertEqual(viewModel.description, product.description)
        XCTAssertEqual(viewModel.brand, product.brand.name)
        let formattedPrice = formatter.string(from: product.price.value as NSNumber)!
        XCTAssertEqual(viewModel.price, formattedPrice)
        XCTAssertEqual(viewModel.score, product.score)
        XCTAssertEqual(viewModel.inCart, product.inCart ?? false)
    }

    func testProductInWishlist() {
        product.inWishlist = true
        let viewModel = ProductCellViewModel(product: product, currencyFormatter: formatter)
        XCTAssertTrue(viewModel.inWishlist)
    }

    func testProductInCart() {
        product.inCart = true
        let viewModel = ProductCellViewModel(product: product, currencyFormatter: formatter)
        XCTAssertTrue(viewModel.inCart)
    }
}
