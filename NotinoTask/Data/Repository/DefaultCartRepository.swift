//
// Created by Jan Pavlica on 01.12.2022.
//

import Foundation

struct DefaultCartRepository: CartRepository {

    private let storage: CartStorage

    init(storage: CartStorage = DefaultCartStorage()) {
        self.storage = storage
    }

    func getCart() -> [Product] {
        return storage.getCart().map { $0.toDomain() }
    }

    func addToCart(product: Product) {
        storage.addToCart(product: ProductEntity(from: product))
    }

    func removeFromCart(product: Product) {
        storage.removeFromCart(product: ProductEntity(from: product))
    }
}
