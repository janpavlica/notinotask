//
// Created by Jan Pavlica on 01.12.2022.
//

import Foundation

struct DefaultWishlistRepository: WishlistRepository {

    private let storage: WishlistStorage

    init(storage: WishlistStorage = DefaultWishlistStorage()) {
        self.storage = storage
    }

    func getWishlist() -> [Product] {
        return storage.getWishlist().map { $0.toDomain() }
    }

    func addToWishlist(product: Product) {
        storage.addToWishlist(product: ProductEntity(from: product))
    }

    func removeFromWishlist(product: Product) {
        storage.removeFromWishlist(product: ProductEntity(from: product))
    }
}
