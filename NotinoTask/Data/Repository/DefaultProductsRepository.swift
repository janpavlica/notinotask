//
// Created by Jan Pavlica on 01.12.2022.
//

import Foundation

struct DefaultProductsRepository: ProductsRepository {

    private let api: ShopAPI
    private let imageServerUrl: String

    init(api: ShopAPI = DefaultShopAPI(), imageServerUrl: String = "https://i.notino.com/detail_zoom/") {
        let uiTesting = ProcessInfo.processInfo.arguments.contains("UITesting")
        if uiTesting {
            self.api = MockShopAPI()
        } else {
            self.api = api
        }
        self.imageServerUrl = imageServerUrl
    }

    func getProducts() async throws -> [Product] {

        var products = try await api.getProducts().toDomain()
        products = products.map({ product in
            var copy = product
            copy.imageUrl = imageServerUrl + "\(product.imageUrl)"
            return copy
        })
        return products
    }
}
