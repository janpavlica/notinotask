//
// Created by Jan Pavlica on 01.12.2022.
//

import Foundation

final class MockShopAPI: ShopAPI {

    func getProducts() async throws -> ProductsDTO {

        let result = ProductsDTO(vpProductByIds: MockUtils.getProductsDTO())
        return result
    }
}
