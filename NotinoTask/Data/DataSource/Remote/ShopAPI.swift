//
// Created by Jan Pavlica on 01.12.2022.
//

import Foundation

protocol ShopAPI {

    func getProducts() async throws -> ProductsDTO
}
