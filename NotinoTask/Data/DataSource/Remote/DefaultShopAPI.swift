//
// Created by Jan Pavlica on 01.12.2022.
//

import Foundation

final class DefaultShopAPI: ShopAPI {

    private let networking: Networking
    private let endpointUrl: String


    init(networking: Networking = URLSession.shared,
         endpointUrl: String = "https://my-json-server.typicode.com/cernfr1993/notino-assignment/db") {
        self.networking = networking
        self.endpointUrl = endpointUrl
    }

    func getProducts() async throws -> ProductsDTO {
        do {
            guard let url = URL(string: endpointUrl) else {
                throw NetworkError.invalidURL
            }

            let (data, response) = try await networking.data(from: url)
            guard let response = response as? HTTPURLResponse else {
                throw NetworkError.noResponse
            }

            switch response.statusCode {
            case 200...299:

                guard let response = try? JSONDecoder().decode(ProductsDTO.self, from: data) else {
                    throw NetworkError.decode
                }
                return response

            default:
                throw NetworkError.unknown
            }
        } catch URLError.Code.notConnectedToInternet {
            throw NetworkError.offline
        }
    }
}
