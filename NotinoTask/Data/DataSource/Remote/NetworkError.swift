//
// Created by Jan Pavlica on 01.12.2022.
//

import Foundation

enum NetworkError: Error {
    case invalidURL
    case noResponse
    case noData
    case decode
    case offline
    case unknown

    var customMessage: String {
        switch self {
        case .offline:
            return "Jste offline. Připojte se prosím k internetu."
        default:
            return "Došlo k chybě. Zkuste to prosím znovu."
        }
    }
}
