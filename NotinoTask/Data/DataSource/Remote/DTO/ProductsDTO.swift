//
// Created by Jan Pavlica on 01.12.2022.
//

import Foundation

struct ProductsDTO: Decodable, Encodable {
    let vpProductByIds: [ProductDTO]
}

extension ProductsDTO {
    func toDomain() -> [Product] {
        vpProductByIds.map { $0.toDomain() }
    }
}
