//
// Created by Jan Pavlica on 03.12.2022.
//

import Foundation

struct ReviewSummaryDTO: Decodable, Encodable {
    let score: Int
    let averageRating: Decimal
}
