//
// Created by Jan Pavlica on 03.12.2022.
//

import Foundation

struct BrandDTO: Decodable, Encodable {
    let id: String
    let name: String
}

extension BrandDTO {
    func toDomain() -> Brand {
        .init(id: id, name: name)
    }
}
