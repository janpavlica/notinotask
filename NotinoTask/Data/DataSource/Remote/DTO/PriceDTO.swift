//
// Created by Jan Pavlica on 03.12.2022.
//

import Foundation

struct PriceDTO: Decodable, Encodable {
    let value: Decimal
    let currency: String
}

extension PriceDTO {
    func toDomain() -> Price {
        .init(value: value, currency: currency)
    }
}
