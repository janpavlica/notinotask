//
// Created by Jan Pavlica on 01.12.2022.
//

import Foundation

struct ProductDTO: Decodable, Encodable {
    let productId: Int64
    let name: String
    let annotation: String
    let brand: BrandDTO
    let price: PriceDTO
    let reviewSummary: ReviewSummaryDTO
    let imagePath: String

    private enum CodingKeys: String, CodingKey {
        case productId
        case name
        case annotation
        case brand
        case price
        case reviewSummary
        case imagePath = "imageUrl"
    }
}

extension ProductDTO {
    func toDomain() -> Product {
        .init(id: productId, name: name, description: annotation, brand: brand.toDomain(),
                price: price.toDomain(), score: reviewSummary.score, imageUrl: imagePath)
    }
}
