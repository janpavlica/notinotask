//
// Created by Jan Pavlica on 07.12.2022.
//

import Foundation

final class DefaultWishlistStorage: WishlistStorage {

    private let wishlistKey = "notino.wishlist"
    private var userDefaults: UserDefaults

    init(userDefaults: UserDefaults = UserDefaults()) {
        self.userDefaults = userDefaults
    }

    func getWishlist() -> [ProductEntity] {
        if let itemsData = userDefaults.object(forKey: wishlistKey) as? Data {
            if let items = try? JSONDecoder().decode([ProductEntity].self, from: itemsData) {
                return items
            }
        }
        return []
    }

    func addToWishlist(product: ProductEntity) {
        var items = getWishlist()
        if !items.contains(where: { $0.id == product.id }) {
            items.append(product)
            if let encoded = try? JSONEncoder().encode(items) {
                userDefaults.set(encoded, forKey: wishlistKey)
            }
        }
    }

    func removeFromWishlist(product: ProductEntity) {
        var items = getWishlist()
        if let index = items.firstIndex(where: { $0.id == product.id }) {
            items.remove(at: index)
            if let encoded = try? JSONEncoder().encode(items) {
                userDefaults.set(encoded, forKey: wishlistKey)
            }
        }
    }

    func removeAll() {
        userDefaults.set(nil, forKey: wishlistKey)
    }
}
