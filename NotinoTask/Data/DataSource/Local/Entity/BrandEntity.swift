//
// Created by Jan Pavlica on 07.12.2022.
//

import Foundation

struct BrandEntity: Decodable, Encodable {
    let id: String
    let name: String

    init(from brand: Brand) {
        self.id = brand.id
        self.name = brand.name
    }
}

extension BrandEntity {
    func toDomain() -> Brand {
        .init(id: id, name: name)
    }
}
