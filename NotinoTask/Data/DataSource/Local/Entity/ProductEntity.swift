//
// Created by Jan Pavlica on 07.12.2022.
//

import Foundation

struct ProductEntity: Decodable, Encodable {
    let id: Int64
    let name: String
    let description: String
    let brand: BrandEntity
    let price: PriceEntity
    let score: Int
    var imageUrl: String

    init(from product: Product) {
        self.id = product.id
        self.name = product.name
        self.description = product.description
        self.brand = BrandEntity(from: product.brand)
        self.price = PriceEntity(from: product.price)
        self.score = product.score
        self.imageUrl = product.imageUrl
    }
}

extension ProductEntity {
    func toDomain() -> Product {
        .init(id: id, name: name, description: description, brand: brand.toDomain(),
                price: price.toDomain(), score: score, imageUrl: imageUrl)
    }
}
