//
// Created by Jan Pavlica on 07.12.2022.
//

import Foundation

struct PriceEntity: Decodable, Encodable {
    let value: Decimal
    let currency: String

    init(from price: Price) {
        self.value = price.value
        self.currency = price.currency
    }
}

extension PriceEntity {
    func toDomain() -> Price {
        .init(value: value, currency: currency)
    }
}
