//
// Created by Jan Pavlica on 07.12.2022.
//

import Foundation

protocol WishlistStorage {
    func getWishlist() -> [ProductEntity]
    func addToWishlist(product: ProductEntity)
    func removeFromWishlist(product: ProductEntity)
    func removeAll()
}
