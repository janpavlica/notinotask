//
// Created by Jan Pavlica on 08.12.2022.
//

import Foundation

protocol CartStorage {
    func getCart() -> [ProductEntity]
    func addToCart(product: ProductEntity)
    func removeFromCart(product: ProductEntity)
    func removeAll()
}
