//
// Created by Jan Pavlica on 08.12.2022.
//

import Foundation

final class DefaultCartStorage: CartStorage {

    private let cartKey = "notino.cart"
    private var userDefaults: UserDefaults

    init(userDefaults: UserDefaults = UserDefaults()) {
        self.userDefaults = userDefaults
    }

    func getCart() -> [ProductEntity] {
        if let itemsData = userDefaults.object(forKey: cartKey) as? Data {
            if let items = try? JSONDecoder().decode([ProductEntity].self, from: itemsData) {
                return items
            }
        }
        return []
    }

    func addToCart(product: ProductEntity) {
        var items = getCart()
        if !items.contains(where: { $0.id == product.id }) {
            items.append(product)
            if let encoded = try? JSONEncoder().encode(items) {
                userDefaults.set(encoded, forKey: cartKey)
            }
        }
    }

    func removeFromCart(product: ProductEntity) {
        var items = getCart()
        if let index = items.firstIndex(where: { $0.id == product.id }) {
            items.remove(at: index)
            if let encoded = try? JSONEncoder().encode(items) {
                userDefaults.set(encoded, forKey: cartKey)
            }
        }
    }

    func removeAll() {
        userDefaults.set(nil, forKey: cartKey)
    }
}
