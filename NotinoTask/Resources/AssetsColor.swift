//
// Created by Jan Pavlica on 06.12.2022.
//

import UIKit

enum AssetsColor: String {
    case InkPrimary
    case InkSecondary
    case InkTertiary
    case Pink
    case Gray
    case GrayLight
}

extension UIColor {
    static func appColor(_ name: AssetsColor) -> UIColor? {
        return UIColor(named: name.rawValue)
    }
}
