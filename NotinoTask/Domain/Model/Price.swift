//
// Created by Jan Pavlica on 03.12.2022.
//

import Foundation

struct Price {
    let value: Decimal
    let currency: String
}
