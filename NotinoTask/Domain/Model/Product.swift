//
// Created by Jan Pavlica on 01.12.2022.
//

import Foundation

struct Product: Identifiable {
    let id: Int64
    let name: String
    let description: String
    let brand: Brand
    let price: Price
    let score: Int
    var imageUrl: String
    var inWishlist: Bool?
    var inCart: Bool?

    init(id: Int64, name: String, description: String, brand: Brand, price: Price, score: Int, imageUrl: String) {
        self.id = id
        self.name = name
        self.description = description
        self.brand = brand
        self.price = price
        self.score = score
        self.imageUrl = imageUrl
    }
}
