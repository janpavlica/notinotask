//
// Created by Jan Pavlica on 03.12.2022.
//

import Foundation

struct Brand: Identifiable {
    let id: String
    let name: String
}
