//
// Created by Jan Pavlica on 01.12.2022.
//

import Foundation

protocol ProductsRepository {
    func getProducts() async throws -> [Product]
}
