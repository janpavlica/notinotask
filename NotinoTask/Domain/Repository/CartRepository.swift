//
// Created by Jan Pavlica on 01.12.2022.
//

import Foundation

protocol CartRepository {
    func getCart() -> [Product]
    func addToCart(product: Product)
    func removeFromCart(product: Product)
}
