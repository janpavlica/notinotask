//
// Created by Jan Pavlica on 01.12.2022.
//

import Foundation

protocol WishlistRepository {
    func getWishlist() -> [Product]
    func addToWishlist(product: Product)
    func removeFromWishlist(product: Product)
}
