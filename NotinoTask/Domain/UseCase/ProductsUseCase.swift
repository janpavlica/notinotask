//
// Created by Jan Pavlica on 01.12.2022.
//

import Foundation

struct ProductsUseCase {

    private let productsRepository: ProductsRepository
    private let wishlistRepository: WishlistRepository
    private let cartRepository: CartRepository

    init(productsRepository: ProductsRepository = DefaultProductsRepository(),
         wishlistRepository: WishlistRepository = DefaultWishlistRepository(),
         cartRepository: CartRepository = DefaultCartRepository()) {
        self.productsRepository = productsRepository
        self.wishlistRepository = wishlistRepository
        self.cartRepository = cartRepository
    }

    func getProducts() async throws -> [Product] {
        var products = try await productsRepository.getProducts()
        let wishlist = wishlistRepository.getWishlist()
        let cart = cartRepository.getCart()

        products = products.map({ product in
            var copy = product
            copy.inWishlist = wishlist.contains(where: { $0.id == product.id })
            copy.inCart = cart.contains(where: { $0.id == product.id })
            return copy
        })

        let sortedProducts = products.sorted(by: { $0.name < $1.name })
        return sortedProducts
    }

    func toggleWishlist(product: Product) {
        let wishlist = wishlistRepository.getWishlist()
        let exists = wishlist.contains(where: { $0.id == product.id })
        if exists {
            wishlistRepository.removeFromWishlist(product: product)
        } else {
            wishlistRepository.addToWishlist(product: product)
        }
    }

    func toggleCart(product: Product) {
        let cart = cartRepository.getCart()
        let exists = cart.contains(where: { $0.id == product.id })
        if exists {
            cartRepository.removeFromCart(product: product)
        } else {
            cartRepository.addToCart(product: product)
        }
    }
}
