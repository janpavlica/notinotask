//
// Created by Jan Pavlica on 02.12.2022.
//

import UIKit

class InfoView: UIView {

    lazy private var messageLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    lazy var actionButton: UIButton = {
        let button = UIButton(type: .contactAdd)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()

    var message: String? {
        didSet {
            messageLabel.text = message
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupView() {
        addSubview(messageLabel)

        let safeGuide = safeAreaLayoutGuide
        messageLabel.centerXAnchor.constraint(equalTo: safeGuide.centerXAnchor).isActive = true
        messageLabel.centerYAnchor.constraint(equalTo: safeGuide.centerYAnchor).isActive = true
    }
}
