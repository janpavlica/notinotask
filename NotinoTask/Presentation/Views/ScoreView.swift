//
// Created by Jan Pavlica on 07.12.2022.
//

import UIKit

class ScoreView: UIView {

    var maxScore = 5
    var starWidth = 12
    var starMargin = 2

    var width: Float {
        Float((maxScore * (starWidth + starMargin)) - starMargin)
    }

    var score: Int? {
        didSet {
            updateStars(score ?? 0)
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupView() {
        for i in 0...(maxScore - 1) {
            let frame = CGRect(x: i * (starWidth + starMargin), y: 0, width: starWidth, height: starWidth)
            let star = UIImageView(frame: frame)
            star.image = UIImage(named: "Star")
            addSubview(star)
        }
    }

    private func updateStars(_ score: Int) {
        if(score > 0) {
            for i in 0...(score - 1) {
                if let star = subviews[i] as? UIImageView {
                    star.image = UIImage(named: "Star-filled")
                }
            }
        } else {
            for i in 0...(maxScore - 1) {
                if let star = subviews[i] as? UIImageView {
                    star.image = UIImage(named: "Star")
                }
            }
        }
    }
}
