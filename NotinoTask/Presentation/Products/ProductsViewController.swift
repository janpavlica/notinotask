//
//  ProductsViewController.swift
//  NotinoTask
//
//  Created by Jan Pavlica on 30.11.2022.
//

import UIKit
import Combine

class ProductsViewController: UIViewController {

    private let viewModel: ProductsViewModel
    private var cancellable = Set<AnyCancellable>()

    private let refreshControl = UIRefreshControl()
    private var productsView: UICollectionView!
    private var infoView: InfoView!

    let flowLayout: CustomCollectionViewFlowLayout = {

        let layout = CustomCollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 8
        layout.minimumLineSpacing = 20
        layout.sectionInset = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        layout.itemSize = UICollectionViewFlowLayout.automaticSize
        layout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        return layout
    }()

    init(viewModel: ProductsViewModel = ProductsViewModel()) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .systemBackground
        title = viewModel.title

        setupInfoView()
        setupProductsView()
        setupBindings()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.loadProducts()
    }

    override func viewWillDisappear(_ animated: Bool) {
        cancellable.removeAll()
        super.viewWillDisappear(animated)
    }

    @objc
    private func refreshProducts(_ sender: Any) {
        viewModel.loadProducts()
    }

    private func setupInfoView() {
        infoView = InfoView(frame: view.bounds)
    }

    private func setupProductsView() {
        productsView = UICollectionView(frame: view.bounds, collectionViewLayout: flowLayout)
        productsView.register(ProductCell.self, forCellWithReuseIdentifier: ProductCell.identifier)
        productsView.translatesAutoresizingMaskIntoConstraints = false
        productsView.accessibilityIdentifier = "productsCollectionView"

        productsView.delegate = self
        productsView.dataSource = self

        refreshControl.addTarget(self, action: #selector(refreshProducts(_:)), for: .valueChanged)
        productsView.alwaysBounceVertical = true
        productsView.refreshControl = refreshControl
    }

    private func setupBindings() {
        viewModel.$products
                .receive(on: RunLoop.main)
                .sink(receiveValue: { _ in
                    self.productsView.reloadData()
                })
                .store(in: &cancellable)

        let stateValueHandler: (ProductsViewModelState) -> Void = { [weak self] state in
            self?.refreshControl.endRefreshing()
            switch state {
            case .loading:
                self?.showInfo("Načítám data...")
            case .empty:
                self?.showInfo("Seznam produktů je prázdný.")
            case .finished:
                self?.showProducts()
            case .error(let error):
                self?.showInfo(error.customMessage)
            }
        }

        viewModel.$state
                .receive(on: RunLoop.main)
                .sink(receiveValue: stateValueHandler)
                .store(in: &cancellable)
    }

    private func showInfo(_ message: String) {
        removeAllSubviews()
        view.addSubview(infoView)

        infoView.message = message
    }

    private func showProducts() {
        removeAllSubviews()
        view.addSubview(productsView)

        let safeGuide = view.safeAreaLayoutGuide
        productsView.topAnchor.constraint(equalTo: safeGuide.topAnchor).isActive = true
        productsView.leadingAnchor.constraint(equalTo: safeGuide.leadingAnchor).isActive = true
        productsView.trailingAnchor.constraint(equalTo: safeGuide.trailingAnchor).isActive = true
        productsView.bottomAnchor.constraint(equalTo: safeGuide.bottomAnchor).isActive = true
    }

    private func removeAllSubviews() {
        view.subviews.forEach({ $0.removeFromSuperview() })
    }
}

extension ProductsViewController: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        viewModel.products.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductCell.identifier, for: indexPath) as? ProductCell else {
            return UICollectionViewCell()
        }
        let product = viewModel.products[indexPath.item]
        cell.viewModel = ProductCellViewModel(product: product, currencyFormatter: FormatterUtils.currencyFormatter)
        cell.index = indexPath

        cell.toggleWishlist.compactMap{$0}
                .sink { [self] index in
                    let product = viewModel.products[indexPath.item]
                    viewModel.toggleWishlist(product: product)
                }.store(in: &cell.cancellable)

        cell.toggleCart.compactMap{$0}
                .sink { [self] index in
                    let product = viewModel.products[indexPath.item]
                    viewModel.toggleCart(product: product)
                }.store(in: &cell.cancellable)

        return cell
    }
}

extension ProductsViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let availableWidth = collectionView.bounds.width - flowLayout.minimumInteritemSpacing - flowLayout.sectionInset.left - flowLayout.sectionInset.right
        let itemDimension = floor(availableWidth / 2)
        return CGSize(width: itemDimension, height: view.bounds.height)
    }
}
