//
// Created by Jan Pavlica on 01.12.2022.
//

import Foundation
import Combine

final class ProductCellViewModel {

    var inWishlist: Bool {
        product.inWishlist ?? false
    }

    var imageUrl: String {
        product.imageUrl
    }

    var name: String {
        product.name
    }

    var description: String {
        product.description
    }

    var brand: String {
        product.brand.name
    }

    var price: String {
        guard let formattedPrice = currencyFormatter.string(from: product.price.value as NSNumber) else {
            return ""
        }
        return formattedPrice
    }

    var score: Int {
        product.score
    }

    var inCart: Bool {
        product.inCart ?? false
    }

    private var product: Product
    private var currencyFormatter: NumberFormatter

    init(product: Product, currencyFormatter: NumberFormatter) {
        self.product = product
        currencyFormatter.currencyCode = product.price.currency
        self.currencyFormatter = currencyFormatter
    }
}
