//
// Created by Jan Pavlica on 01.12.2022.
//

import Foundation
import Combine

enum ProductsViewModelState: Equatable {
    case loading
    case empty
    case finished
    case error(NetworkError)
}

final class ProductsViewModel {

    let title = "Produkty"

    @Published private(set) var products: [Product] = []
    @Published private(set) var state: ProductsViewModelState = .loading

    private let useCase: ProductsUseCase
    private var cancellable: AnyCancellable?

    init(useCase: ProductsUseCase = ProductsUseCase()) {
        self.useCase = useCase
    }

    func loadProducts() {
        state = .loading
        cancellable = getProducts()
                .receive(on: DispatchQueue.main)
                .sink { completion in
                    switch completion {
                    case .failure(let error):
                        self.state = .error(error as! NetworkError)
                        print(error)
                    default: break
                    }
                } receiveValue: { products in
                    if products.isEmpty {
                        self.state = .empty
                    } else {
                        self.state = .finished
                        self.products = products
                    }
                }
    }

    func toggleWishlist(product: Product) {
        useCase.toggleWishlist(product: product)
        if let index = self.products.firstIndex(where: { $0.id == product.id }) {
            self.products[index].inWishlist = !(product.inWishlist ?? false)
        }
    }

    func toggleCart(product: Product) {
        useCase.toggleCart(product: product)
        if let index = self.products.firstIndex(where: { $0.id == product.id }) {
            self.products[index].inCart = !(product.inCart ?? false)
        }
    }
}

extension ProductsViewModel {

    private func getProducts() -> Future<[Product], Error> {
        return Future() { [self] promise in
            Task {
                do {
                    let products = try await useCase.getProducts()
                    promise(.success(products))
                } catch {
                    promise(.failure(error))
                }
            }
        }
    }
}
