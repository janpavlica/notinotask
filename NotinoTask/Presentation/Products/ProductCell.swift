//
// Created by Jan Pavlica on 30.11.2022.
//

import UIKit
import Combine

class ProductCell: UICollectionViewCell {

    static let identifier = "ProductCell"

    var viewModel: ProductCellViewModel! {
        didSet {
            setupViewModel()
        }
    }

    private var wishlistButtonConfig: UIButton.Configuration = {
        var configuration = UIButton.Configuration.plain()
        configuration.baseBackgroundColor = .clear
        configuration.contentInsets = NSDirectionalEdgeInsets(top: 16, leading: 16, bottom: 16, trailing: 16)
        return configuration
    }()

    lazy private var wishlistButton: UIButton = {
        let button = UIButton(configuration: wishlistButtonConfig)
        button.setImage(UIImage(named: "Heart"), for: .normal)
        button.setImage(UIImage(named: "Heart-filled"), for: .selected)
        button.addTarget(self, action: #selector(toggleWishlist(_:)), for: .touchUpInside)
        return button
    }()

    lazy private var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        return imageView
    }()

    lazy private var brandLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        label.textColor = UIColor.appColor(AssetsColor.InkTertiary)
        label.textAlignment = .center
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        return label
    }()

    lazy private var nameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        label.textAlignment = .center
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        return label
    }()

    lazy private var descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        label.textColor = UIColor.appColor(AssetsColor.InkSecondary)
        label.textAlignment = .center
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        return label
    }()

    lazy private var scoreView: ScoreView = {
        let view = ScoreView(frame: CGRect.zero)
        return view
    }()

    lazy private var priceLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        label.textAlignment = .center
        label.numberOfLines = 1
        return label
    }()

    private var cartButtonConfig: UIButton.Configuration = {
        var configuration = UIButton.Configuration.plain()
        configuration.baseBackgroundColor = .clear
        configuration.titleTextAttributesTransformer = UIConfigurationTextAttributesTransformer { incoming in
            var outgoing = incoming
            outgoing.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
            return outgoing
        }
        configuration.contentInsets = NSDirectionalEdgeInsets(top: 8, leading: 12, bottom: 8, trailing: 12)
        return configuration
    }()

    lazy private var cartButton: UIButton = {
        let button = UIButton(configuration: cartButtonConfig)
        button.setTitle("Do košíku", for: .normal)
        button.setTitleColor(UIColor.appColor(AssetsColor.InkPrimary), for: .normal)
        button.setTitle("V košíku", for: .selected)
        button.setTitleColor(UIColor.appColor(AssetsColor.InkTertiary), for: .selected)
        button.layer.cornerRadius = 2
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.appColor(AssetsColor.Gray)?.cgColor
        button.addTarget(self, action: #selector(toggleCart(_:)), for: .touchUpInside)
        return button
    }()

    var cancellable = Set<AnyCancellable>()

    // Multiple Publishers for cell
    let toggleWishlist = PassthroughSubject<IndexPath?, Never>()
    let toggleCart = PassthroughSubject<IndexPath?, Never>()
    var index: IndexPath?

    override init(frame: CGRect) {
        super.init(frame: frame)

        addSubViews()
        setupConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        cancellable.removeAll()
    }

    private func addSubViews() {
        let subviews = [wishlistButton, imageView, brandLabel, nameLabel,
                        descriptionLabel, scoreView, priceLabel, cartButton]
        subviews.forEach {
            contentView.addSubview($0)
            $0.translatesAutoresizingMaskIntoConstraints = false
        }
    }

    private func setupConstraints() {
        wishlistButton.widthAnchor.constraint(equalToConstant: 48).isActive = true
        wishlistButton.heightAnchor.constraint(equalToConstant: 48).isActive = true
        wishlistButton.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        wishlistButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true

        imageView.widthAnchor.constraint(equalToConstant: contentView.bounds.width).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 160).isActive = true
        imageView.topAnchor.constraint(equalTo: wishlistButton.bottomAnchor).isActive = true

        brandLabel.widthAnchor.constraint(equalToConstant: contentView.bounds.width).isActive = true
        brandLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 12).isActive = true

        nameLabel.widthAnchor.constraint(equalToConstant: contentView.bounds.width).isActive = true
        nameLabel.topAnchor.constraint(equalTo: brandLabel.bottomAnchor, constant: 4).isActive = true

        descriptionLabel.widthAnchor.constraint(equalToConstant: contentView.bounds.width).isActive = true
        descriptionLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor).isActive = true

        scoreView.widthAnchor.constraint(equalToConstant: CGFloat(scoreView.width)).isActive = true
        scoreView.heightAnchor.constraint(equalToConstant: CGFloat(scoreView.starWidth)).isActive = true
        scoreView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        scoreView.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: 12).isActive = true

        priceLabel.widthAnchor.constraint(equalToConstant: contentView.bounds.width).isActive = true
        priceLabel.topAnchor.constraint(equalTo: scoreView.bottomAnchor, constant: 12).isActive = true

        cartButton.heightAnchor.constraint(equalToConstant: 36).isActive = true
        cartButton.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        cartButton.topAnchor.constraint(equalTo: priceLabel.bottomAnchor, constant: 12).isActive = true
        cartButton.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -12).isActive = true
    }

    private func setupViewModel() {
        wishlistButton.isSelected = viewModel.inWishlist
        imageView.loadImageWithUrl(urlString: viewModel.imageUrl)
        brandLabel.text = viewModel.brand
        nameLabel.text = viewModel.name
        descriptionLabel.text = viewModel.description
        scoreView.score = viewModel.score
        priceLabel.text = viewModel.price
        cartButton.isSelected = viewModel.inCart
        cartButton.sizeToFit()
    }

    @objc private func toggleWishlist(_ sender: UIButton?) {
        self.toggleWishlist.send(index)
    }

    @objc private func toggleCart(_ sender: UIButton?) {
        self.toggleCart.send(index)
    }
}