//
// Created by Jan Pavlica on 15.12.2022.
//

import Foundation

class FormatterUtils {

    static let currencyFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 2
        formatter.currencySymbol = "Kč"
        return formatter
    }()
}
