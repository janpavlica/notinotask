//
// Created by Jan Pavlica on 08.12.2022.
//

import Foundation

class MockUtils {

    private static let productDTO_1: ProductDTO = {
        let brand = BrandDTO(id: "1234", name: "Brand name")
        let price = PriceDTO(value: 100, currency: "CZK")
        let reviewSummary = ReviewSummaryDTO(score: 4, averageRating: 3.8)
        let product = ProductDTO(productId: 123456, name: "Product name", annotation: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit",
                brand: brand, price: price, reviewSummary: reviewSummary, imagePath: "product1.png")
        return product
    }()

    private static let productDTO_2: ProductDTO = {
        let brand = BrandDTO(id: "4321", name: "Some brand")
        let price = PriceDTO(value: 1000, currency: "CZK")
        let reviewSummary = ReviewSummaryDTO(score: 5, averageRating: 4.9)
        let product = ProductDTO(productId: 654321, name: "Example product", annotation: "Lorem ipsum dolor sit amet",
                brand: brand, price: price, reviewSummary: reviewSummary, imagePath: "product2.png")
        return product
    }()

    private static let productDTO_3: ProductDTO = {
        let brand = BrandDTO(id: "1234", name: "Brand name")
        let price = PriceDTO(value: 550, currency: "CZK")
        let reviewSummary = ReviewSummaryDTO(score: 2, averageRating: 1.75)
        let product = ProductDTO(productId: 987654, name: "Some product", annotation: "Lorem ipsum dolor sit amet, consectetuer adipiscing",
                brand: brand, price: price, reviewSummary: reviewSummary, imagePath: "product3.png")
        return product
    }()

    static func getProductsResponse() -> ProductsDTO {
        return ProductsDTO(vpProductByIds: getProductsDTO())
    }

    static func getProductsDTO() -> [ProductDTO] {
        return [ productDTO_1, productDTO_2, productDTO_3 ]
    }

    static func getProductDTO() -> ProductDTO {
        return productDTO_1
    }

    static func getProducts() -> [Product] {
        return getProductsResponse().toDomain()
    }

    static func getProduct() -> Product {
        return productDTO_1.toDomain()
    }
}
